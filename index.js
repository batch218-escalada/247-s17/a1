/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

    function printUserInfo() {
        let name = prompt('What is your name?');
        let age = prompt('How old are you?');
        let address = prompt('Where do you live?');

        console.log('Hello, ' + name);
        console.log('You are ' + age + ' years old.');
        console.log('You live in ' + address);
    };

    printUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

    function showMyTopFiveBands() {
        console.log('1. Linkin Park');
        console.log('2. RADWIMPS');
        console.log('3. We The Kings');
        console.log('4. Parokya Ni Edgar');
        console.log('5. Brownman Revival');
    };

    showMyTopFiveBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

    function printFavoriteMovies() {
        console.log('Avatar (2009)');
        console.log('Rotten Tomatoes Rating: 82%');
        console.log('The Lord of the Rings: The Return of the King (2003)');
        console.log('Rotten Tomatoes Rating: 93%');
        console.log('The Lord of the Rings: The Fellowship of the Ring (2001)');
        console.log('Rotten Tomatoes Rating: 91%');
        console.log('The Lord of the Rings: The Two Towers (2002)');
        console.log('Rotten Tomatoes Rating: 95%');
        console.log('Avengers: Endgame (2019)');
        console.log('Rotten Tomatoes Rating: 94%');
    };

    printFavoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


printFriends();

// console.log(friend1);
// console.log(friend2);